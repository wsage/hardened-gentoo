echo "Specify the block device that will serve as a boot partition" 
echo "Warning: the following operations will destroy your data on that device forever"
read gentoo_boot
echo "Your device is ${gentoo_boot}"
echo "Specify the block device that will serve as the rest of the disk"
echo "Warning: the following operations will destroy your data on that device forever"
read gentoo_rest
echo "Your device is ${gentoo_rest}"
echo ""

echo "Boot partition is ${gentoo_boot}"
echo "The rest of the disk is ${gentoo_rest}"

encpv="enc-pv3"
vg="vg3"

# It is recommended to check if the device is not in use
# with this command: sfdisk -R /dev/sdx, where x is the letter of the drive to partition
sfdisk -R ${gentoo_boot}
device_not_busy=$?

if [[ ${device_not_busy} -eq 1 ]] ; then
	echo "The boot device is busy. Unmount it and try again later"
	exit 1
fi

sfdisk -R ${gentoo_rest}
device_not_busy=$?

if [[ ${device_not_busy} -eq 1 ]] ; then
	echo "The rest of the disk is busy. Unmount it and try again later"
	exit 1
fi

echo "Do you want to fill the whole device with random data?" 
select yn in "Yes" "No"; do
	case $yn in
		Yes ) echo "Filling ${gentoo_rest} with random data" 
			echo "Run killall -USR1 dd in another terminal to get the status"
			dd if=/dev/urandom of=${gentoo_rest} bs=1M	
			break;;
		No ) break;;
	esac
done

echo "Do you want to create an ext2 filesystem on ${gentoo_boot}?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) echo "Creating ext2 filesystem on ${gentoo_boot}"
			mkfs.ext2 -L GENTOO_BOOT ${gentoo_boot} 
			break;;
		No ) break;;
	esac
done

mkdir -p /mnt/gentoo-autoinstall
mkdir -p /mnt/boot-autoinstall

echo "Mounting ${gentoo_boot} on /mnt/boot-autoinstall"
mount ${gentoo_boot} /mnt/boot-autoinstall

if [[ ! -e /usr/share/syslinux/mbr.bin ]] ; then
	echo "The required file /usr/share/syslinux/mbr.bin doesn't exist on your system"
	echo "Please install syslinux"
	exit 1
fi

# Put the boot sector onto your USB stick
dd bs=404 count=1 conv=notrunc if=/usr/share/syslinux/mbr.bin of=${gentoo_boot}

mkdir /mnt/boot-autoinstall/syslinux

# Create the bootloader configuration file
cp ./configs/syslinux.cfg /mnt/boot-autoinstall/syslinux/syslinux.cfg

# Install syslinux onto your USB stick
extlinux --device ${gentoo_boot} --install /mnt/boot-autoinstall/syslinux

echo "You need to have cryptsetup, lvm, and busybox installed"

if [[ -e /sbin/cryptsetup ]] ; then
	ldd /sbin/cryptsetup
	cryptsetup_statically_linked=$?
	if [[ ${cryptsetup_statically_linked} -eq 0 ]] ; then
		echo "Install a statically linked version of cryptsetup and type YES when done"
		read cryptsetup_ready
	fi
else
	exit 1
fi

if [[ -e /sbin/lvm.static ]] ; then
	ldd /sbin/lvm.static
	lvm_statically_linked=$?
	if [[ ${lvm_statically_linked} -eq 0 ]] ; then
		echo "Install a statically linked version of lvm and type YES when done"
		read lvm_ready
	fi
else
	exit 1
fi

if [[ -e /bin/busybox ]] ; then
	ldd /bin/busybox
	busybox_statically_linked=$?
	if [[ ${busybox_statically_linked} -eq 0 ]] ; then
		echo "Install a statically linked version of busybox and type YES when done"
		read busybox_ready
	fi
else 
	exit 1
fi

# Turn swap off; with swap enabled full disc encryption is a waste of time
swapoff -av

cp /sbin/{cryptsetup,lvm.static} ./initramfs/sbin
cp /bin/busybox ./initramfs/bin
# you'll get a kernel panic on boot otherwise
cp -a /dev/{null,console,tty,random,urandom,loop0} ./initramfs/dev

pushd ./initramfs
# First make a keyfile and mount it on /dev/loop0
dd if=/dev/urandom of=root/key.iso count=1 bs=7M
losetup /dev/loop0 root/key.iso

# Then encrypt /dev/loop0 with cryptsetup
cryptsetup --cipher aes-xts-plain --key-size 512 --hash sha512 --use-random --iter-time 5000 luksFormat /dev/loop0

# Open the device and map it to /dev/mapper/lukskey
cryptsetup luksOpen /dev/loop0 lukskey

dd if=/dev/urandom of=/dev/mapper/lukskey

echo "The operation may take some while. You can type on keyboard or move your mouse to generate more entropy and finish it faster."
dd if=/dev/random of=/dev/mapper/lukskey count=1000 bs=1

find . -print0 | cpio --null -ov --format=newc | gzip -9 > /mnt/boot-autoinstall/my-initramfs.cpio.gz
cp -r . /mnt/boot-autoinstall/initramfs
popd

cryptsetup --cipher aes-xts-plain --key-size 512 --hash sha512 --use-random --iter-time 5000 --uuid=12345678-1234-1234-1234-fede4a166666 --key-file /dev/mapper/lukskey luksFormat ${gentoo_rest} 

cryptsetup --key-file /dev/mapper/lukskey luksOpen UUID=12345678-1234-1234-1234-fede4a166666 ${encpv}

# Clean up the loop device
cryptsetup luksClose lukskey
losetup -d /dev/loop0

# Create a physical volume
pvcreate /dev/mapper/${encpv}

# And a volume group
vgcreate ${vg} /dev/mapper/${encpv}

# Now add the logical volumes
lvcreate -L 1G -n root ${vg}
lvcreate -L 10G -n usr ${vg}
lvcreate -L 1G -n opt ${vg}
lvcreate -L 16G -n var ${vg}
lvcreate -L 2G -n swap ${vg}
lvcreate -n home --extents 100%FREE ${vg}

# Format them
mkfs.ext4 -L GENTOO_ROOT -j -m 1 -O dir_index,filetype,sparse_super /dev/${vg}/root
mkfs.ext4 -L GENTOO_HOME -j -m 1 -O dir_index,filetype,sparse_super /dev/${vg}/home
mkfs.ext4 -L GENTOO_USR -j -m 1 -O dir_index,filetype,sparse_super /dev/${vg}/usr
mkfs.ext4 -L GENTOO_OPT -j -m 1 -O dir_index,filetype,sparse_super /dev/${vg}/opt
mkfs.ext4 -L GENTOO_VAR -j -m 1 -O dir_index,filetype,sparse_super /dev/${vg}/var
mkswap -L GENTOO_SWAP /dev/${vg}/swap

mount /dev/${vg}/root /mnt/gentoo-autoinstall
mkdir /mnt/gentoo-autoinstall/home
mount /dev/${vg}/home /mnt/gentoo-autoinstall/home
mkdir /mnt/gentoo-autoinstall/usr
mount /dev/${vg}/usr /mnt/gentoo-autoinstall/usr
mkdir /mnt/gentoo-autoinstall/opt
mount /dev/${vg}/opt /mnt/gentoo-autoinstall/opt
mkdir /mnt/gentoo-autoinstall/var
mount /dev/${vg}/var /mnt/gentoo-autoinstall/var

echo "You need to download and put the latest amd64 hardened stage3 tarball in the downloads directory"

tar xvjpf ./downloads/stage3-*.tar.bz2 -C /mnt/gentoo-autoinstall/
tar xvf ./downloads/portage-latest.tar.xz -C /mnt/gentoo-autoinstall/usr/

# copy over DNS information
cp -L /etc/resolv.conf /mnt/gentoo-autoinstall/etc/

# copy some useful settings
cat /etc/portage/make.conf > /mnt/gentoo-autoinstall/etc/portage/make.conf
cat /etc/portage/package.use > /mnt/gentoo-autoinstall/etc/portage/package.use
cat /etc/portage/package.mask > /mnt/gentoo-autoinstall/etc/portage/package.mask
cat /etc/portage/package.keywords > /mnt/gentoo-autoinstall/etc/portage/package.keywords
cp /etc/wvdial.conf /mnt/gentoo-autoinstall/etc

# mount /proc and /dev
mount -t proc none /mnt/gentoo-autoinstall/proc
mount --rbind /sys /mnt/gentoo-autoinstall/sys
mount --rbind /dev /mnt/gentoo-autoinstall/dev

# select your timezone so that your system know where it's located
chroot /mnt/gentoo-autoinstall echo "Europe/Moscow" > /mnt/gentoo-autoinstall/etc/timezone
# reconfigure the timezone-data package, which will update the /etc/localtime file for us, 
# based on the /etc/timezone entry
chroot /mnt/gentoo-autoinstall emerge --config sys-libs/timezone-data

# reload shell environment
chroot /mnt/gentoo-autoinstall env-update

echo "Install the necessary software"

# make sure it points to the right thing
configure_kernel() {
	emerge -av hardened-sources
	cd /usr/src/linux
	make menuconfig
	make
}

# configure_kernel

install_packets() {
	chroot /mnt/gentoo-autoinstall emerge -v syslog-ng vixie-cron dhcpcd ppp wvdial iptables lynx tor torsocks vlock terminus-font
	chroot /mnt/gentoo-autoinstall rc-update add syslog-ng default
	chroot /mnt/gentoo-autoinstall rc-update add vixie-cron default
	chroot /mnt/gentoo-autoinstall rc-update add iptables boot
	chroot /mnt/gentoo-autoinstall /etc/init.d/iptables save
	chroot /mnt/gentoo-autoinstall rc-update add tor default
	chroot /mnt/gentoo-autoinstall rc-update add consolefont boot
	chroot /mnt/gentoo-autoinstall rc-update add udev sysinit
	chroot /mnt/gentoo-autoinstall rc-update add udev-mount sysinit
}

install_packets

# set the root password
chroot /mnt/gentoo-autoinstall passwd

# in order to call it from different parts of the script
finish() {
	umount -l /mnt/gentoo-autoinstall/dev
	umount -l /mnt/gentoo-autoinstall/sys
	umount -l /mnt/gentoo-autoinstall/proc
	umount /dev/mapper/${vg}-var
	umount /dev/mapper/${vg}-opt
	umount /dev/mapper/${vg}-usr
	umount /dev/mapper/${vg}-home
	umount /dev/mapper/${vg}-root
	umount ${gentoo_boot}
	vgchange -a n
	cryptsetup luksClose ${encpv}

	rm -r /mnt/gentoo-autoinstall
	rm -r /mnt/boot-autoinstall
	exit 0
}

finish
