#!/bin/bash
# A basic stateful firewall for a workstation or laptop that isn't running any
# network services like a web server, SMTP server, ftp server, etc, 
# but acts as a home router.

if [[ "$1" = "start" ]] ; then

#--------------------------------------------------------------------
# Any connection that you initiate will get back in through
# the firewall. However, any unsolicited connection 
# that comes in from the Internet will be dropped,
# unless it's related to an existing connection that you initiated.
#
# URL: http://en.gentoo-wiki.com/wiki/Iptables/ \
# Iptables_and_stateful_firewalls
#
# URL: https://wiki.archlinux.org/index.php/Simple_Stateful_Firewall
#--------------------------------------------------------------------

	echo "Starting firewall..."
	iptables -F
	iptables -P INPUT DROP
	# iptables -A INPUT -i eth0 -j ACCEPT
	iptables -A INPUT -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT
	iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
	iptables -A INPUT -p tcp -j REJECT --reject-with tcp-reset
	iptables -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable

elif [[ "$1" = "route" ]] ; then

#--------------------------------------------------------------------
# NAT (a.k.a. IP-masquerading)
# Allows to connect multiple computers in a private LAN 
# to the internet. 
# Workstation can act as a router using these commands.
#
# URL: http://www.gentoo.org/doc/en/home-router-howto.xml
#--------------------------------------------------------------------

iptables -t nat -F
iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE

#-------------------------------------------------------------------- 
# Prior to masquerading, the packets are routed via the filter
# table's FORWARD chain.
# Allowed outbound: New, established and related connections
# Allowed inbound : Established and related connections
#
# URL: http://www.linuxhomenetworking.com/wiki/index.php/ \
# Quick_HOWTO_:_Ch14_:_Linux_Firewalls_Using_iptables \
# #Masquerading_.28Many_to_One_NAT.29
#--------------------------------------------------------------------

	iptables -P FORWARD DROP
	iptables -A FORWARD -t filter -o ppp0 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
	iptables -A FORWARD -t filter -i ppp0 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

elif [[ "$1" = "stop" ]] ; then

	echo "Stopping firewall..."
	iptables -F INPUT
	iptables -P INPUT ACCEPT
fi
